```mermaid
sequenceDiagram

participant u as User
participant cp as TPV
participant ep as TPV Receiver
participant cc as TPV Controller
participant jm as Journey Manager

u ->> cp : Blah Blah Blah

opt Direction == 'inbound'
  Note over cp,cc : Blah Blah Blah
  cp -->> ep : Blah Blah Blah

ep -->> cc  : Blah Blah Blah

alt Blah Blah Blah
    cc -->> ep : Blah Blah Blah

else 
    alt Blah Blah Blah
      cc -->> ep : Blah Blah Blah
    else Blah Blah Blah
      cc -->> ep : Blah Blah Blah
      Note over ep,cc : Blah Blah Blah
    end

end

end

ep -->> jm : Blah Blah Blah
jm -->> ep : Blah Blah Blah

alt Blah Blah Blah
  ep -->> cp : Blah Blah Blah
  cp ->> u :Blah Blah Blah

else Blah Blah Blah
  ep -->> cp : Blah Blah Blah
  cp ->> u : Blah Blah Blah
  Note over u,ep: TBlah Blah Blah

else Blah Blah Blah
  ep -->> cp : Blah Blah Blah
  cp ->> u : Blah Blah Blah

end
```